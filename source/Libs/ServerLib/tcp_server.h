//
// Created by ozzadar on 2021-08-22.
//

#pragma once

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/thread.hpp>
#include <tcp_connection.h>
#include <functional>
#include <optional>
#include <unordered_set>
#include <QObject>

//#include "mainwindow.h"
//#include "ui_mainwindow.h"


namespace io = boost::asio;

enum class IPV {
    V4,
    V6
};

class TCPServer : public QObject {
    Q_OBJECT
    using OnJoinHandler = std::function<void(TCPConnection::pointer)>;
    using OnLeaveHandler = std::function<void(TCPConnection::pointer)>;
    using OnClientMessageHandler = std::function<void(std::string)>;

public:
    TCPServer();
    TCPServer(IPV ipv, int port);
    void init(IPV ipv, int port);
    void init(std::string raw_ip_address, int port);

    int Run();
    void Broadcast(const std::string& message);
    void Close();

    boost::thread * tRun;

private:
    void startAccept();

signals:
    void ParseMessageSignal(std::string str);
    void ServerOngoingSignal();
    void OnJoinSignal();
    void OnLeaveSignal();
    void ClientMessageSignal(const std::string& str);

public slots:
    void XmlFileParsedSlot(std::string);
    void ServerClosedSlot();

public:
    OnJoinHandler OnJoin;
    OnLeaveHandler OnLeave;
    OnClientMessageHandler OnClientMessage;

public:
    inline const  std::unordered_set<TCPConnection::pointer> GetConnections() {return _connections;};

private:
    IPV _ipVersion;
    int _port;

    io::io_context _ioContext;
    io::ip::tcp::acceptor  _acceptor;
    std::optional<io::ip::tcp::socket> _socket;
    std::unordered_set<TCPConnection::pointer> _connections {};
};



