//
// Created by ozzadar on 2021-08-22.
//

#include "tcp_server.h"
#include <iostream>

using boost::asio::ip::tcp;

TCPServer::TCPServer() : _ipVersion(IPV::V4), _port(1337),
                         _acceptor(_ioContext, tcp::endpoint(_ipVersion == IPV::V4 ? tcp::v4() : tcp::v6(), _port)) {
}

TCPServer::TCPServer(IPV ipv, int port) : _ipVersion(ipv), _port(port),
    _acceptor(_ioContext, tcp::endpoint(_ipVersion == IPV::V4 ? tcp::v4() : tcp::v6(), _port)) {
}

void TCPServer::init(IPV ipv, int port)
{
    _ipVersion = ipv;
    _port = port;
    _acceptor = io::ip::tcp::acceptor(_ioContext, tcp::endpoint(_ipVersion == IPV::V4 ? tcp::v4() : tcp::v6(), _port));
}

void TCPServer::init(std::string raw_ip_address, int port)
{
    _port = port;
    _acceptor = io::ip::tcp::acceptor(_ioContext, tcp::endpoint(boost::asio::ip::address::from_string(raw_ip_address), _port));
}

int TCPServer::Run() {
    // слабое место: в try бессмысленно вызывать сигнал
    // Сигнал не вызывается. Поэтому в случае ошибки,
    // слот для этого сигнала все равно выполнится
    emit ServerOngoingSignal();

    try {
        startAccept();
        _ioContext.run();
        emit ServerOngoingSignal();
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return -1;
    }
    return 0;
}

void TCPServer::Broadcast(const std::string &message) {
    for (auto& connection : _connections) {
        connection->Post(message);
    }
}

void TCPServer::startAccept() {
    _socket.emplace(_ioContext);

    // asynchronously accept the connection
    _acceptor.async_accept(*_socket, [this](const boost::system::error_code& error){
        auto connection = TCPConnection::Create(std::move(*_socket));

        if (OnJoin) {
            OnJoin(connection);
            emit OnJoinSignal();
        }

        _connections.insert(connection);
        if (!error) {
            connection->Start(
                [this](const std::string& message) { if (OnClientMessage) OnClientMessage(message); emit ParseMessageSignal(message);  },
                [&, weak =std::weak_ptr(connection)] {
                    if (auto shared = weak.lock(); shared && _connections.erase(shared)) {
                        if (OnLeave)
                        {
                            OnLeave(shared);
                            emit OnLeaveSignal();
                        }
                    }
                }
            );
        }

        startAccept();
    });
}

void TCPServer::XmlFileParsedSlot(std::string xml_parsed_results)
{
    this->Broadcast(xml_parsed_results);
}

void TCPServer::ServerClosedSlot()
{
    // close connections
    // TBD

    // TBD: close socket and service - almost done
//    boost::system::error_code ec;
//    this->_socket->close();
//    this->_socket->release();
//    this->_ioContext.stop();
//    if (ec)
//    {
//        // An error occurred.
//        std::cout<< ec.what() << std::endl;
//    }

//    // close thread
//    this->tRun->join();
}


