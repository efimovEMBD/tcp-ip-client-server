//
// Created by ozzadar on 2021-09-21.
//

#include "tcp_client.h"
#include <iostream>

    TCPClient::TCPClient(const std::string &address, int port) : _socket(_ioContext) {
        io::ip::tcp::resolver resolver {_ioContext};
        _endpoints = resolver.resolve(address, std::to_string(port));
    }

    TCPClient::TCPClient() : _socket(_ioContext) {
        io::ip::tcp::resolver resolver {_ioContext};
        _endpoints = resolver.resolve("127.0.0.1", std::to_string(1337));
    }

    void TCPClient::init(const std::string& raw_ip_address, int port)
    {
        io::ip::tcp::resolver resolver {_ioContext};
        _endpoints = resolver.resolve(raw_ip_address, std::to_string(port));
    }

    void TCPClient::Run() {
        io::async_connect(_socket, _endpoints, [this](boost::system::error_code ec, io::ip::tcp::endpoint ep) {
            if (!ec)
            {
                isConnected_ = true;
                emit Connected();
                asyncRead();
            }
            else
            {

            }
        });
        _ioContext.run();
    }

    void TCPClient::Stop() {
        boost::system::error_code ec;
        _socket.close(ec);

        if (ec) {
            // process error
        }
    }

    void TCPClient::Post(const std::string &msg) {
        bool queueIdle = _outgoingMessages.empty();
        _outgoingMessages.push(msg);

        if (queueIdle) {
            asyncWrite();
        }
    }

    void TCPClient::asyncRead() {
        io::async_read_until(_socket, _streamBuf, "\n", [this](boost::system::error_code ec, size_t bytesTransferred) {
           onRead(ec, bytesTransferred);
        });
    }

    void TCPClient::onRead(boost::system::error_code ec, size_t bytesTransferred) {
        if (ec) {
            Stop();
            isConnected_ = false;
            emit Unconnected();
            return;
        }

        std::stringstream msgFromServer;
        msgFromServer << std::istream{&_streamBuf}.rdbuf();
        OnMessage(msgFromServer.str());
        emit GotMessage_signal(msgFromServer.str());
        asyncRead();
    }

    void TCPClient::asyncWrite() {
        io::async_write(_socket, io::buffer(_outgoingMessages.front()), [this](boost::system::error_code ec, size_t bytesTransferred) {
            onWrite(ec, bytesTransferred);
        });
    }

    void TCPClient::onWrite(boost::system::error_code ec, size_t bytesTransferred) {
        if (ec) {
            Stop();
            return;
        }

        _outgoingMessages.pop();

        if (!_outgoingMessages.empty()) {
            asyncWrite();
        }
    }
