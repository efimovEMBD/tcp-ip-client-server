#include <string>
#include <boost/date_time.hpp>
#include <QTime>
#include <QCoreApplication>

std::string GetUtcTime()
{
    // boost version:
    // Get the current time in UTC Timezone
    return boost::posix_time::to_simple_string(boost::posix_time::second_clock::universal_time());

    // stl version
    // TBD: will find out someday ....
}

void delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

struct
{
    std::string version = "N/D";
    std::string from= "N/D";
    std::string to= "N/D";
    std::string id= "N/D";
    std::string color= "000000";
    std::string text= "N/D";
    std::string image= "N/D";
} xmlData;


