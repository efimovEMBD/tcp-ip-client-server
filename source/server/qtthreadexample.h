#ifndef QTTHREADEXAMPLE_H
#define QTTHREADEXAMPLE_H

#include <QThread>
#include <QMutexLocker>

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>



class Worker : public QObject {
    Q_OBJECT

public:
    Worker();
    ~Worker();

    void SetEditTextPane(QTextEdit *);

public slots:
    void process();

signals:
    void finished();
    void error(QString err);

private:
    QTextEdit *textBoxOnClientMessage_;

};

//class Thread : public QThread
//{
//    Q_OBJECT

//public:
//    Thread():m_stop(false)
//    {}

//public slots:
//    void stop()
//    {
////        qDebug()<<"Thread::stop called from main thread: "<<currentThreadId();
//        QMutexLocker locker(&m_mutex);
//        m_stop=true;
//    }

//private:
//    QMutex m_mutex;
//    bool m_stop;

//    void run()
//    {
////        qDebug()<<"From worker thread: "<<currentThreadId();
//        while (1) {
//            {
//                QMutexLocker locker(&m_mutex);
//                if (m_stop) break;
//            }
//            msleep(10);
//        }
//    }
//};

#endif // QTTHREADEXAMPLE_H
