#include "qtthreadexample.h"


extern bool isMessageReceived;

Worker::Worker() {
    // you could copy data from constructor arguments to internal variables here.
}

Worker::~Worker() {
    // free resources
}

void Worker::SetEditTextPane(QTextEdit * pane)
{
    textBoxOnClientMessage_ = pane;
}

void Worker::process() {

//    while(true)
    {
        if (isMessageReceived)
        {
            isMessageReceived = false;
            textBoxOnClientMessage_->setPlainText("Got msg from client\r\n");
        }
    }

//    qDebug("Hello World!");
    emit finished();
}
