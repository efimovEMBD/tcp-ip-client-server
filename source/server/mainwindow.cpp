#include "mainwindow.h"
//#include "./ui_mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFuture>
#include <QCoreApplication>
#include <QFileDialog>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QtConcurrent/QtConcurrent>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/date_time.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/system/error_code.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind/bind.hpp>

#include <fstream>
#include "rapidxml.hpp"
#include <qtthreadexample.h>

using namespace std;
using namespace rapidxml;

using namespace boost::placeholders;
using namespace boost::asio;
using namespace std;
using boost::enable_shared_from_this;
using boost::shared_ptr;
using boost::asio::io_service;
using boost::asio::ip::tcp;
using boost::system::error_code;

#include <tcp_server.h>
#include "utils.hpp"
TCPServer server;

xml_document<> doc;
xml_node<> * root_node = NULL;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    this->setWindowTitle("XML Parser v0.0.1 (Server)");
    ui->tEditSetIP->setText("127.0.0.1");
    ui->tEditSetPort->setText("1337");

    // сигнал о получении сообщения от клиента
    connect(&server, &TCPServer::ParseMessageSignal, this, &MainWindow::ShowMessageSlot);

    // сигнал о парсинге файла
    connect(this, &MainWindow::ParseXmlSignal, &server, &TCPServer::XmlFileParsedSlot);

    // сигнал о создании сервера
    connect(&server, &TCPServer::ServerOngoingSignal, this, &MainWindow::ServerRunningSlot);

    // сигнал о подключении очередного клиента
    connect(&server, &TCPServer::OnJoinSignal, this, &MainWindow::OnJoinSlot);

    // сигнал об отключении клиента
    connect(&server, &TCPServer::OnLeaveSignal, this, &MainWindow::OnLeaveSlot);

    // сигнал о закрытии сервера пользователем
    connect(this, &MainWindow::ServerClosedSignal, &server, &TCPServer::ServerClosedSlot);


    server.OnJoin = [this](TCPConnection::pointer server) {
        // если спорно, то сигнал вызывается позже этого лямбда-выражения,
        // поэтому можно использовать счётчик, хотя мне такое решение не нравится
        userName.emplace_back(server->GetUsername());
        userCount++;
        std::cout << "User has joined the server: " << server->GetUsername() << std::endl;
    };

    server.OnLeave = [](TCPConnection::pointer server) {
        std::cout << "User has left the server: " << server->GetUsername() << std::endl;
    };

    server.OnClientMessage = [&, this](const std::string& message)
    {
      cout << message;
    };

    db = QSqlDatabase::addDatabase("QSQLITE");
//    db.setHostName("localhost");
    db.setDatabaseName("db.sqlite");
    auto opened = db.open();
    if(!opened) {
        qDebug() << "Cannot open db" << db.lastError().text() ;
    } else {

    }

    if(!db.tables(QSql::AllTables).contains(QLatin1String("person"))) {
        db.exec("create table person (id int primary key, "
                   "firstname varchar(20), lastname varchar(20))");
        db.exec("insert into person values(101, 'Danny', 'Young')");
        db.exec("insert into person values(102, 'Christine', 'Holand')");
        db.exec("insert into person values(103, 'Lars', 'Gordon')");
        db.exec("insert into person values(104, 'Roberto', 'Robitaille')");
        db.exec("insert into person values(105, 'Maria', 'Papadopoulos')");

        db.exec("create table items (id int primary key,"
                                                 "imagefile int,"
                                                 "itemtype varchar(20),"
                                                 "description varchar(100))");
    }
    model = new QSqlTableModel(this, db);
    model->setTable("person");
//    model->setEditStrategy(QSqlTableModel::OnManualSubmit); // on manual user submit
    model->setHeaderData(0, Qt::Horizontal, tr("id"));
    model->setHeaderData(1, Qt::Horizontal, tr("firstname"));
    model->setHeaderData(2, Qt::Horizontal, tr("lastname"));
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->show();

}

void MainWindow::ShowMessageSlot(std::string message)
{
    std::cout << "Client message: " << message;
    ui->textBoxOnClientMessage->append("[CON] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText(QString::fromStdString(message.replace(message.find("\r\n", 0), 2, "")));


    if (message.find("get-#xml#;") != std::string::npos)
    {
        emit ParseXmlSignal("#xml#;" + // ключ начала передачи данных *.xml-файла
                            xmlData.version + "#;" +
                            xmlData.from + "#;" +
                            xmlData.to + "#;" +
                            xmlData.id + "#;" +
                            xmlData.color + "#;" +
                            xmlData.text + "#;" +
                            xmlData.image + "\n"  );
    }
}

void MainWindow::ServerRunningSlot()
{
    ui->textBoxOnClientMessage->append("[SYS] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText("Server created: waiting for connection ...");
    ui->btnOpen->setEnabled(false);
}

void MainWindow::OnJoinSlot()
{
    ui->textBoxOnClientMessage->append("[CON] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText("User" + QString::fromStdString(userName.at(userCount-1)) + " has joined the server ");
}

void MainWindow::OnLeaveSlot()
{
    ui->textBoxOnClientMessage->append("[CON] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText("User <?> has left the server ");
}

void MainWindow::on_btnClose_clicked()
{
    emit ServerClosedSignal();
    ui->textBoxOnClientMessage->append("[SYS] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText("Server closed ");
    ui->btnOpen->setEnabled(true);

    // todo: unconnect previously connected signals and slots
}

void MainWindow::on_btnOpen_clicked()
{
    std::string ip = ui->tEditSetIP->toPlainText().toStdString();
    std::string port = ui->tEditSetPort->toPlainText().toStdString();

    // слабое место: работа с указателями и ссылками
    TCPServer *localServer = &server;
    localServer->init(ip, stoi(port));

    boost::thread* tRunServer = new boost::thread(boost::bind(&TCPServer::Run, &server));
    server.tRun = tRunServer;

}

// слабое место: нет никакого механизма обработки в случае повреждённого файла: не тестировал с другими файлами
// поэтому нужно выбирать файлы с приведённым шаблоном
void MainWindow::on_btnSelectFile_clicked()
{
    QString filename = QFileDialog::getOpenFileName(
        nullptr,
        QObject::tr("Open Document"),
        QDir::currentPath(),
        QObject::tr("Document files (*.xml *.txt);;All files (*.*)"));
    ifstream theFile (filename.toStdString());

    ui->textBoxOnClientMessage->append("[SYS] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText("New '*.xml' file selected: ");
    ui->textBoxOnClientMessage->insertPlainText(filename);

    vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
    buffer.push_back('\0');

    // Parse the buffer
    doc.parse<0>(&buffer[0]);
    std::cout << "File parsed" << std::endl;

    std::string formatVersion;
    std::string xmlFrom;
    std::string xmlTo;
    std::string msgId;
    std::string msgColor;
    std::string msgText;
    std::string msgImage;

    // Find out the root node
    root_node = doc.first_node("root");

    // Iterate over the nodes
    for (xml_node<>* message_node = root_node->first_node("Message"); message_node; message_node = message_node->next_sibling())
    {
        cout << "\nFormat Version = " << message_node->first_attribute("FormatVersion")->value();
        formatVersion = std::string(message_node->first_attribute("FormatVersion")->value());
        xmlData.version = formatVersion;

        cout << "\nFrom = " << message_node->first_attribute("from")->value();
        xmlFrom = std::string(message_node->first_attribute("from")->value());
        xmlData.from = xmlFrom;

        cout << "\nTo = " << message_node->first_attribute("to")->value();
        xmlTo = std::string(message_node->first_attribute("to")->value());
        xmlData.to = xmlTo;

        // Interate over the messages
        for(xml_node<>* msg_node = message_node->first_node("msg"); msg_node; msg_node = msg_node->next_sibling())
        {
            cout << "\nmessage id = " << msg_node->first_attribute("id")->value();
            msgId = std::string(msg_node->first_attribute("id")->value());
            xmlData.id = msgId;

            cout << "\nmessage color = " << msg_node->first_node("text")->first_attribute("color")->value();
            msgColor = std::string(msg_node->first_node("text")->first_attribute("color")->value());
            xmlData.color = msgColor;

            cout << "\nmessage text = " << msg_node->first_node("text")->value();
            msgText = std::string(msg_node->first_node("text")->value());
            xmlData.text = msgText;

            cout << "\nimage = " << msg_node->first_node("image")->value();
            msgImage = std::string(msg_node->first_node("image")->value());
            xmlData.image = msgImage;

            cout << endl;
        }
        cout << endl;
    }

    // showing parsing results
    ui->lblFormatVersion-> setText("FormatVersion: " + QString::fromUtf8(formatVersion));
    ui->lblFrom->setText("From: " + QString::fromUtf8(xmlFrom));
    ui->lblTo->setText("To: " + QString::fromUtf8(xmlTo));
    ui->lblId->setText("ID: " + QString::fromUtf8(msgId));
    ui->lblColor->setText("Color: 0x" + QString::fromStdString(msgColor));
    QString msgColorCode = QString::fromStdString(msgColor);
    ui->lblMessageText->setStyleSheet("QLabel {color : #"+msgColorCode+"; }");
    ui->lblMessageText->setText("Message: " + QString::fromUtf8(msgText));
    QByteArray data = QString::fromStdString(msgImage).toUtf8();
    QPixmap image;
    image.loadFromData(QByteArray::fromBase64(data));
    ui->labelImage->setPixmap(image);

    ui->textBoxOnClientMessage->append("[SYS] ");
    ui->textBoxOnClientMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textBoxOnClientMessage->insertPlainText("Provided '*.xml' file parsed successfully ");

    // по хорошему тут должен быть json хотя-бы или plain-html
    emit ParseXmlSignal("#xml#;" + // ключ начала передачи данных *.xml-файла
                        formatVersion + "#;" +
                        xmlFrom + "#;" +
                        xmlTo + "#;" +
                        msgId + "#;" +
                        msgColor + "#;" +
                        msgText + "#;"  +
                        msgImage + "\n");
}




void MainWindow::on_pbInsertRow_clicked()
{
    model->insertRow(model->rowCount());
}


void MainWindow::on_pbDeleteRow_clicked()
{
    auto *selection = ui->tableView->selectionModel();
    for(auto row : selection->selectedRows()) {
        model->removeRow(row.row());
    }
}

