#include "mainwindow.h"
#include <QApplication>
#include <boost/bind/bind.hpp>
using namespace boost::placeholders;

#include <QThread>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
