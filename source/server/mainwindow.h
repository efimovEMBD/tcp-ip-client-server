#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <string.h>
#include <QtSql>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QSqlDatabase db;
    QSqlQuery *query;
    QSqlTableModel *model;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void init();

    std::vector<std::string> userName;
    int userCount = 0;

public slots:
    void ShowMessageSlot(std::string message);
    void ServerRunningSlot();
//    void ServerKilledSlot();
    void OnJoinSlot();
    void OnLeaveSlot();


signals:
    void ParseXmlSignal(std::string);
    void ServerClosedSignal();

private slots:
    void on_btnOpen_clicked();
    void on_btnSelectFile_clicked();
    void on_btnClose_clicked();

    void on_pbInsertRow_clicked();

    void on_pbDeleteRow_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
