#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QColorDialog>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/system/error_code.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind/bind.hpp>

using namespace boost::placeholders;
using namespace boost::asio;
using namespace std;
using boost::enable_shared_from_this;
using boost::shared_ptr;
using boost::asio::io_service;
using boost::asio::ip::tcp;
using boost::system::error_code;

#include "tcp_client.h"
#include "utils.hpp"
TCPClient client;

vector<string> split (string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    this->setWindowTitle("XML Parser v0.0.1 (Client)");
    ui->tEditSetIP->setText("127.0.0.1");
    ui->tEditSetPort->setText("1337");

    connect(&client, &TCPClient::GotMessage_signal, this, &MainWindow::GotMessage_slot);
    connect(&client, &TCPClient::Unconnected, this, &MainWindow::ServerDown);
    connect(&client, &TCPClient::Unconnected, this, &MainWindow::Connected_slot);
}

void MainWindow::GotMessage_slot(const std::string& message)
{
    ui->textEditOnServerMessage->append("[CON] ");
    ui->textEditOnServerMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textEditOnServerMessage->insertPlainText(QString::fromUtf8(message));
    std::string delimiter = "#;";
    size_t pos = 0;
    vector<string> v = split (message, delimiter);
    if (v.at(0) == "#xml")
    {
        std::string fontColor = "#";
        fontColor += v.at(5);
        QString colcode = QString::fromStdString(fontColor);
        ui->lblFrom->setText("From:" + QString::fromUtf8(v.at(2)));
        ui->lblMessage->setStyleSheet("QLabel {color : "+colcode+"; }");
        ui->lblMessage->setText("Message:" + QString::fromUtf8(v.at(6)));

        // show image
        QByteArray data = QString::fromStdString(v.at(7)).toUtf8();
        QPixmap image;
        image.loadFromData(QByteArray::fromBase64(data));
        ui->lblImage->setPixmap(image);

    }
}

void MainWindow::ServerDown()
{
    ui->textEditOnServerMessage->append("[CON] ");
    ui->textEditOnServerMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textEditOnServerMessage->insertPlainText("Server disconnected in bad way ");

}

void MainWindow::Connected_slot()
{
    ui->textEditOnServerMessage->append("[CON] ");
    ui->textEditOnServerMessage->insertPlainText("[" + QString::fromStdString(GetUtcTime()) + "] ");
    ui->textEditOnServerMessage->insertPlainText("Connected to the server ");
}

void MainWindow::on_btnOpen_clicked()
{
    std::string ip = ui->tEditSetIP->toPlainText().toStdString();
    std::string port = ui->tEditSetPort->toPlainText().toStdString();

    TCPClient* localClient = &client;
    localClient->init(ip, std::stoi(port));


    client.OnMessage = [this](const std::string& message) {
        std::cout << message << std::endl;
    };

    boost::thread* tRunClient = new boost::thread(boost::bind(&TCPClient::Run, &client));
}


void MainWindow::on_btnSelectFile_clicked()
{
    client.Post("get-#xml#;\r\n");
}


void MainWindow::on_pushButton_clicked()
{
    std::string text = ui->lineEdit->text().toStdString();
    client.Post(text + "\r\n");
}

